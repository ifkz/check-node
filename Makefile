build:
	docker-compose build
up:
	docker-compose up -d

up-non-daemon:
	docker-compose up
ps:
	docker-compose ps

logs-node:
	docker-compose logs check-node

start:
	docker-compose start

down:
	docker-compose down

stop:
	docker-compose stop

restart:
	docker-compose restart

test:
	docker-compose exec check-node pytest -v --cov=check_node --cov-report term-missing --cov-fail-under=100 tests/