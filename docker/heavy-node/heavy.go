package main

import (
	"bufio"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/http/httptrace"
	"net/url"
	"strconv"
	"strings"
	"time"
)

type Elapsed struct {
	DNSLookup       float64 `json:"dns_lookup"`
	TLSHandshake    float64 `json:"tls_handshake"`
	ServerProcess   float64 `json:"server_process"`
	ContentTransfer float64 `json:"content_transfer"`
}

type RespAns struct {
	StatusCode    int     `json:"status_code"`
	ContentLength int64   `json:"content_length"`
	Elapsed       Elapsed `json:"elapsed"`
}

type RespAnsWithErr struct {
	Timeout     bool			 `json:"timeout"`
	RespAns     *RespAns         `json:"resp_ans"`
	URL         string           `json:"url"`
	QueriedWhen ISOTime          `json:"queried_when"`
	Err         StringifiedError `json:"err"`
}

type Done struct {
	RespAnsWithErrs []RespAnsWithErr `json:"resp_ans_with_errs"`
}

type ISOTime time.Time

func (t ISOTime) MarshalJSON() ([]byte, error) {
	stamp := "\"" + time.Time(t).UTC().Format(time.RFC3339) + "\""
	return []byte(stamp), nil
}

type StringifiedError struct {
	Error error
}

func (se StringifiedError) MarshalJSON() ([]byte, error) {
	if se.Error == nil {
		return []byte("null"), nil
	}

	marshalled, err := json.Marshal(se.Error.Error())
	if err != nil {
		return nil, err
	}
	return marshalled, nil
}

type PathVarNotSetError struct {
	key         string
	value       string
	nestedError error
}

func (e PathVarNotSetError) Error() string {
	if e.nestedError != nil {
		return fmt.Sprintf(
			"Path var `%v` is not set or equals to `%v` with nested error: `%v`",
			e.key, e.value, e.nestedError)
	}
	return fmt.Sprintf("Path var `%v` is not set or equals to `%v`", e.key, e.value)
}

type URLs []string

func ConcReq(client *http.Client, urlChan chan string, ans chan RespAnsWithErr) {
	cururl := <-urlChan

	currTime := time.Now()
	req, err := http.NewRequest("HEAD", cururl, nil)
	if err != nil {
		ans <- RespAnsWithErr{
			Err:         StringifiedError{err},
			URL:         cururl,
			QueriedWhen: ISOTime(currTime),
		}
		return
	}

	var connect, dns, tlsHandshake time.Time
	var DNSLookup, TLSHandshake, ServerProcess, ContentTransfer time.Duration

	trace := &httptrace.ClientTrace{
		DNSStart: func(dsi httptrace.DNSStartInfo) { dns = time.Now() },
		DNSDone:  func(ddi httptrace.DNSDoneInfo) { DNSLookup = time.Since(dns) },

		TLSHandshakeStart: func() { tlsHandshake = time.Now() },
		TLSHandshakeDone:  func(cs tls.ConnectionState, err error) { TLSHandshake = time.Since(tlsHandshake) },

		ConnectStart: func(network, addr string) { connect = time.Now() },
		ConnectDone:  func(network, addr string, err error) { ServerProcess = time.Since(connect) },

		GotFirstResponseByte: func() { ContentTransfer = time.Since(currTime) },
	}
	req = req.WithContext(httptrace.WithClientTrace(req.Context(), trace))
	req.Header.Set("User-Agent", "Mozilla/5.0")
	req.Header.Set("Connection", "close")
	req.Close = true
	resp, err := client.Do(req)
	if err != nil {
		switch typedError := err.(type) {
		default:
			ans <- RespAnsWithErr{
				Err:         StringifiedError{err},
				URL:         cururl,
				QueriedWhen: ISOTime(currTime),
			}
		case *url.Error:
			ans <- RespAnsWithErr{
				Timeout: 	 typedError.Timeout(),
				URL:         cururl,
				Err:         StringifiedError{typedError},
				QueriedWhen: ISOTime(currTime),
			}
		}
	} else {
		defer resp.Body.Close()
		ans <- RespAnsWithErr{
			URL: cururl,
			RespAns: &RespAns{
				StatusCode:    resp.StatusCode,
				ContentLength: resp.ContentLength,
				Elapsed: Elapsed{
					DNSLookup:       DNSLookup.Seconds(),
					TLSHandshake:    TLSHandshake.Seconds(),
					ContentTransfer: ContentTransfer.Seconds(),
					ServerProcess:   ServerProcess.Seconds(),
				},
			},
			Err: StringifiedError{nil},
			QueriedWhen: ISOTime(currTime),
		}
	}
}

func ParseUrls(body io.Reader) (*URLs, error) {
	urls := make(URLs, 0)

	scanner := bufio.NewScanner(body)
	for scanner.Scan() {
		urls = append(urls, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return &urls, nil
}

func CreateClient(timeout time.Duration, loadbound int) *http.Client {

	transport := &http.Transport{
		MaxIdleConns:       loadbound,
		IdleConnTimeout:    timeout,
		DisableCompression: true,
		DisableKeepAlives:  true,
		TLSClientConfig:    &tls.Config{InsecureSkipVerify: true},
	}

	return &http.Client{
		Timeout:       timeout,
		CheckRedirect: func(req *http.Request, via []*http.Request) error { return http.ErrUseLastResponse },
		Transport:     transport,
	}

}

func MakeRequest(client *http.Client, urls *URLs, loadbound int) *Done {
	var urlChan = make(chan string, loadbound)
	var ans = make(chan RespAnsWithErr)
	for _, cururl := range *urls {
		urlChan <- cururl
		go ConcReq(client, urlChan, ans)
	}

	respAnsesWithErrs := make([]RespAnsWithErr, 0)

	for i := 0; i < len(*urls); i++ {
		respAnsWithErr := <-ans
		respAnsesWithErrs = append(respAnsesWithErrs, respAnsWithErr)
	}

	return &Done{respAnsesWithErrs}
}

func ParsePath(values url.Values) (time.Duration, int, error) {
	var parse = func(parseKey string) (int, error) {
		keys, ok := values[parseKey]
		if !ok {
			return 0, PathVarNotSetError{parseKey, strings.Join(keys, " "), nil}
		} else {
			key, err := strconv.Atoi(keys[0])
			if err != nil {
				return 0, PathVarNotSetError{parseKey, string(key), err}
			} else {
				return key, nil
			}
		}
	}
	timeoutParsed, err := parse("timeout")
	if err != nil {
		return 0, 0, err
	}
	loadboundParsed, err := parse("loadbound")
	if err != nil {
		return 0, 0, err
	}

	return time.Duration(timeoutParsed) * time.Second, loadboundParsed, nil

}

// This handler requires url path to have at least two variables `timeout`
// and `loadbound` whenever check-node sends the request to the heavy-node.
// Example: heavy/heavy-process?timeout=60&loadbound=7000
func processorHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {

		timeout, loadbound, err := ParsePath(r.URL.Query())
		if err != nil {
			_ = fmt.Errorf("can't parse pathvars %v", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		urls, err := ParseUrls(r.Body)
		if err != nil {
			_ = fmt.Errorf("can't read body %v", err)
			http.Error(w, "can't read body", http.StatusBadRequest)
			return
		}

		client := CreateClient(timeout, loadbound)
		done := MakeRequest(client, urls, loadbound)
		ans, err := json.Marshal(done)
		if err != nil {
			_ = fmt.Errorf("can't marshall %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		_, err = fmt.Fprintf(w, string(ans))
	} else {
		http.NotFound(w, r)
	}
}

func main() {
	http.HandleFunc("/heavy-process", processorHandler)
	log.Panic(http.ListenAndServe(":8080", nil))
}
