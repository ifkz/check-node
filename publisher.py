import asyncio
import aio_pika
from aio_pika import RobustConnection


async def main(loop):
    connection: RobustConnection = await aio_pika.connect_robust(
        "amqp://rabbitmq:rabbitmq@127.0.0.1/", loop=loop
    )

    routing_key = "check:"

    channel: aio_pika.Channel = await connection.channel()

    await channel.default_exchange.publish(
        aio_pika.Message(body='{"action": "WHOAMI"}'.encode()),
        routing_key=routing_key,
    )

    await connection.close()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
