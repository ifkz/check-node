from tests.fixtures import *  # noqa
from tests.rabbit_models.fixtures import *  # noqa
from tests.db.fixtures import *  # noqa
from tests.responses_fixtures import *  # noqa
