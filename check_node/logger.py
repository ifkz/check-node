import logging

from check_node.config import get_env
from check_node.utils import get_project_root

env = get_env()

formatter = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"


def get_logger(name: str) -> logging.Logger:
    logging.basicConfig(
        level=env.log_level(),
        format=formatter,
        filename=f"{get_project_root()}/data/check-node.log",
        filemode="w+",
    )
    logger = logging.getLogger(name)

    if env.logger_stdio:
        stream_handler = logging.StreamHandler()
        stream_handler.setFormatter(logging.Formatter(formatter))
        stream_handler.setLevel(env.log_level())
        logger.addHandler(stream_handler)

    return logger
