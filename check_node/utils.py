from datetime import datetime, timezone
from pathlib import Path


def get_project_root():
    return Path(__file__).parent.parent


def get_datetime_now_by_utc() -> datetime:
    return datetime.now(tz=timezone.utc)
