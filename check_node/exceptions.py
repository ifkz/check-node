from typing import Optional

from check_node.rabbit.models import RabbitActionType


class NotOkStatusCode(Exception):
    def __init__(self, code, base_url: str):
        self.code = code
        self.message = f"didn't respond with OK status: {base_url}"


class URLIsNotValid(Exception):
    def __init__(self, url: Optional[str]):
        self.message = f"couldn't parse url: {url}"


class ConnectionIsNotSet(Exception):
    def __init__(self):
        self.message = "connection is not set"


class SessionIsNotSet(Exception):
    def __init__(self):
        self.message = "session is not set"


class UnparseableEntity(Exception):
    def __init__(self, action: RabbitActionType, payload):
        self.message = f"payload for action {action.name} is unparseable"
        self.payload = payload
        self.action = action
