import asyncio
import time
from functools import wraps
from random import random
from ssl import SSLContext
from typing import Any, List, Optional, Tuple, Callable
from urllib.parse import urlparse, ParseResult

import aiohttp

import jsons
from aiohttp import ClientSession, ClientError
from aioify import aioify
from ipwhois import IPWhois
from scapy.layers.inet import IP, ICMP, TracerouteResult, traceroute
from scapy.plist import PacketList
from scapy.sendrecv import sr1
from scapy.volatile import RandShort

from check_node.utils import get_datetime_now_by_utc
from check_node.exceptions import (
    NotOkStatusCode,
    URLIsNotValid,
    SessionIsNotSet,
)
from check_node.logger import get_logger
from check_node.db.models import Scheme, URLCheckDetails, CheckFrequency
from check_node.models import (
    WrapRespAnsWithErr,
    IPDetails,
    URLDetails,
    rawe_to_ucd,
)

logger = get_logger(__name__)

client_exceptions = (
    aiohttp.ClientOSError,
    aiohttp.ServerDisconnectedError,
    aiohttp.ClientConnectorError,
    aiohttp.ClientResponseError,
    aiohttp.ClientPayloadError,
    aiohttp.ServerTimeoutError,
)


def is_session_set(method: Callable):
    @wraps(method)
    def check_session_set(*args, **kwargs):
        ck: CheckKit = args[0]
        if getattr(ck, "session", None) is not None:
            return method(*args, **kwargs)
        else:
            raise SessionIsNotSet()

    return check_session_set


class CheckKit:
    session: ClientSession

    def __init__(
        self,
        heavy_node_url: str,
        session: Optional[ClientSession] = None,
        request_timeout: int = 30,
        loadbound: int = 10000,
        sem: asyncio.Semaphore = asyncio.Semaphore(10000),
        ssl_ctx: SSLContext = None,
        verify_ssl: bool = True
    ):
        self._heavy_node_url = heavy_node_url
        if session:
            self.session = session
        self._request_timeout = request_timeout
        self._loadbound = loadbound
        self._sem = sem
        self.ssl_ctx = ssl_ctx
        self.verify_ssl = verify_ssl

    @staticmethod
    def parse_url(url: Optional[str]) -> URLDetails:
        if url is None:
            raise URLIsNotValid(url)
        try:
            result: ParseResult = urlparse(url)
        except Exception:
            logger.warning(url)
            raise URLIsNotValid(url)

        if result.scheme != "" and result.scheme.lower() not in [
            "http",
            "https",
        ]:
            logger.warning(url)
            raise URLIsNotValid(url)

        http_route: str = ""
        https_route: str = ""
        hostname: str = ""
        if result.scheme == "":
            http_result = urlparse("http://" + url)
            http_route = http_result.geturl()
            https_result = urlparse("https://" + url)
            https_route = https_result.geturl()
            hostname = http_result.netloc
        elif result.scheme in ["https", "http"]:
            result = result._replace(scheme="http")
            http_route = result.geturl()
            result = result._replace(scheme="https")
            https_route = result.geturl()
            hostname = result.netloc

        return URLDetails(hostname, http_route, https_route)

    async def _raw_get_url(self, url) -> Tuple[int, Optional[int], float]:
        async with self._sem:
            start = time.time()
            kwargs = {
                "allow_redirects": False,
                "timeout": self._request_timeout,
                "headers": {"User-Agent": "Mozilla/5.0", "Connection": "close"},
                "raise_for_status": False,
            }
            if self.ssl_ctx:
                kwargs["ssl"] = self.ssl_ctx
            else:
                kwargs["verify_ssl"] = self.verify_ssl

            response = await self.session.head(url, **kwargs)
            async with response:
                end = time.time()
                return response.status, response.content_length, end - start

    async def _generic_get_url(
        self,
        hostname: str,
        url: str,
        scheme: Scheme,
        check_frequency: CheckFrequency,
        retry=3,
        exc: Exception = None,
    ) -> URLCheckDetails:
        elapsed: float = self._request_timeout
        status = 0
        content_length = 0
        if retry <= 0:
            return URLCheckDetails.failed(
                hostname,
                scheme,
                url,
                0.0,
                0.0,
                0.0,
                0.0,
                check_frequency,
                repr(exc),
            )
        try:
            status, content_length, elapsed = await self._raw_get_url(  # type: ignore
                url  # type: ignore
            )  # type: ignore
        except client_exceptions as exc:
            await asyncio.sleep(2 * random())
            return await self._generic_get_url(
                hostname, url, scheme, check_frequency, retry - 1, exc
            )
        except asyncio.TimeoutError as exc:
            return URLCheckDetails.failed(
                hostname,
                scheme,
                url,
                0.0,
                0.0,
                0.0,
                elapsed,
                check_frequency,
                repr(exc),
            )
        except Exception as exc:
            logger.info(msg=f"unexpected error occured: {url}")
            logger.error(repr(exc))
            return URLCheckDetails.failed(
                hostname,
                scheme,
                url,
                0,
                0,
                0,
                elapsed,
                check_frequency,
                repr(exc),
            )
        return URLCheckDetails(
            hostname,
            scheme,
            url,
            True,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            elapsed,
            check_frequency,
            content_length or 0,
            status,
            None,
        )

    @is_session_set
    async def generic_get_url(
        self,
        hostname: str,
        url: str,
        scheme: Scheme,
        check_frequency: CheckFrequency,
    ) -> URLCheckDetails:
        return await self._generic_get_url(
            hostname, url, scheme, check_frequency
        )

    @is_session_set
    async def get_url(
        self, urld: URLDetails, check_frequency=CheckFrequency.LOW
    ):

        list_of_details: List[URLCheckDetails] = []
        list_of_details.extend(
            await asyncio.gather(
                self._generic_get_url(
                    urld.hostname,
                    urld.http_route,
                    Scheme.HTTP,
                    check_frequency,
                ),
                self._generic_get_url(
                    urld.hostname,
                    urld.https_route,
                    Scheme.HTTPS,
                    check_frequency,
                ),
            )
        )
        return list_of_details

    @aioify
    def rdap(self, address: str) -> dict:
        return IPWhois(address).lookup_rdap()

    async def get_my_ip(self) -> Optional[str]:
        try:
            async with self.session.get("https://api.ipify.org/") as response:
                if response.status == 200:
                    return await response.text()
                else:
                    raise NotOkStatusCode(
                        response.status, response.url.human_repr()
                    )
        except (
            NotOkStatusCode,
            LookupError,
            RuntimeError,
            ValueError,
            TypeError,
            ClientError,
            TimeoutError,
        ) as e:
            logger.exception("cannot determine node ip", exc_info=e)
            return None

    async def whoami(self) -> IPDetails:
        try:
            res: Optional[str] = await self.get_my_ip()
            # todo: better to raise an exception here
            if res is None:
                return IPDetails()
            else:
                whois_dict = await self.rdap(res)
                if whois_dict is None:
                    return IPDetails()
                else:
                    return IPDetails(
                        res,
                        whois_dict["asn_country_code"],
                        whois_dict["asn_description"],
                        whois_dict["asn"],
                    )
        except NotOkStatusCode as e:
            logger.exception("welp", exc_info=e)
            return IPDetails()

    async def heavy_request(
        self, urls: str, check_frequency: CheckFrequency
    ) -> List[URLCheckDetails]:
        response = await self.session.post(
            f"{self._heavy_node_url}/heavy-process"
            f"?loadbound={self._loadbound}"
            f"&timeout={self._request_timeout}",
            data=urls,
            timeout=None,
        )
        async with response:
            answer = await response.text("utf-8")
            done = jsons.loads(answer, WrapRespAnsWithErr)
            rawes = done.resp_ans_with_errs
            return [
                rawe_to_ucd(rawe, check_frequency, self._request_timeout)
                for rawe in rawes
            ]

    # TODO: don't know how and why to use for a while :D
    @staticmethod
    @aioify
    def ping(host: str, timeout: int = 10):
        icmp = IP(dst=host) / ICMP()
        res = sr1(icmp, timeout=timeout)
        if res is None:
            print("down")
        else:
            print(res.src)

    # TODO: don't know how and why to use for a while :D
    @staticmethod
    @aioify
    def tracert(
        target: List[str],
        dport: int = 80,
        sport: RandShort = RandShort(),
        l4: Any = None,
        filter: Any = None,
        verbose: Any = None,
        timeout: int = 2,
        minttl=1,
        maxttl=30,
        **kwargs,
    ) -> Tuple[TracerouteResult, PacketList]:
        return traceroute(
            target=target,
            dport=dport,
            sport=sport,
            l4=l4,
            minttl=minttl,
            maxttl=maxttl,
            timeout=timeout,
            filter=filter,
            verbose=verbose,
            **kwargs,
        )

    @property
    def request_timeout(self):
        return self._request_timeout
