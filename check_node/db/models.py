from dataclasses import dataclass, asdict
from datetime import datetime, timedelta
from enum import Enum, auto
from typing import Dict, TypeVar, Optional

from check_node.config import read_freq_params, write_freq_params
from check_node.utils import get_datetime_now_by_utc


class AutoEnum(Enum):
    def _generate_next_value_(name, start, count, last_values):
        return name

    def translate(self, *args, **kwargs):
        return self.value


class Scheme(AutoEnum):
    HTTP = auto()
    HTTPS = auto()


@dataclass(frozen=True)
class TimeInterval:
    weeks: float = 0.0
    days: float = 0.0
    hours: float = 0.0
    minutes: float = 0.0
    seconds: float = 0.0
    microseconds: float = 0.0
    milliseconds: float = 0.0

    def as_timedelta(self) -> timedelta:
        return timedelta(**asdict(self))

    def get_timeinterval(self) -> "TimeInterval":
        return self


class CheckFrequency(AutoEnum):
    HIGH = auto()
    LOW = auto()

    def translate(self, *args, **kwargs):
        return self.value

    def get_interval(self) -> TimeInterval:
        return TimeInterval(**read_freq_params()[self.value])

    def set_interval(self, time_interval: TimeInterval):
        freq_params: Dict[str, Dict[str, float]] = read_freq_params()
        freq_params[self.value] = asdict(time_interval)
        write_freq_params(freq_params)


@dataclass(frozen=True, unsafe_hash=True)
class URLCheckDetails:
    hostname: str
    scheme: Scheme
    url: str
    accessible: bool
    last_check_time_by_utc: datetime
    elapsed_dns_lookup: float
    elapsed_tls_handshake: float
    elapsed_server_process: float
    elapsed_content_transfer: float
    check_frequency: CheckFrequency
    content_length: int = 0
    status_code: int = 0
    exc: Optional[str] = None

    @staticmethod
    def failed(
        hostname: str,
        scheme: Scheme,
        url: str,
        elapsed_dns_lookup: float,
        elapsed_tls_handshake: float,
        elapsed_server_process: float,
        elapsed_content_transfer: float,
        check_frequency: CheckFrequency,
        exc: str,
        last_check_time_by_utc: Optional[datetime] = None,
    ) -> "URLCheckDetails":
        return URLCheckDetails(
            hostname,
            scheme,
            url,
            False,
            last_check_time_by_utc
            if last_check_time_by_utc is not None
            else get_datetime_now_by_utc(),
            elapsed_dns_lookup,
            elapsed_tls_handshake,
            elapsed_server_process,
            elapsed_content_transfer,
            check_frequency,
            -1,
            -1,
            exc,
        )


T = TypeVar("T", bound=URLCheckDetails)
