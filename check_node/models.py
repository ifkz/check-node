from dataclasses import dataclass
from datetime import datetime
from typing import Optional, List
from urllib.parse import ParseResult, urlparse

from check_node.db.models import CheckFrequency, URLCheckDetails, Scheme


@dataclass(frozen=True)
class Elapsed:
    dns_lookup: float
    tls_handshake: float
    server_process: float
    content_transfer: float


@dataclass(frozen=True)
class RespAns:
    status_code: int
    content_length: int
    elapsed: Elapsed


@dataclass(frozen=True)
class RespAnsWithErr:
    timeout: bool
    resp_ans: Optional[RespAns]
    url: str
    queried_when: datetime
    err: Optional[str]


@dataclass(frozen=True)
class WrapRespAnsWithErr:
    resp_ans_with_errs: List[RespAnsWithErr]


@dataclass(frozen=True)
class IPDetails:
    ip: str = ""
    country_code: str = ""
    isp: str = ""
    asn_id: str = ""


@dataclass(frozen=True)
class URLDetails:
    hostname: str
    http_route: str
    https_route: str


def rawe_to_ucd(
    rawe: RespAnsWithErr, check_frequency: CheckFrequency, timeout: int
) -> URLCheckDetails:
    result: ParseResult = urlparse(rawe.url)
    assert result.hostname is not None
    if rawe.err is None and rawe.resp_ans is not None:
        return URLCheckDetails(
            result.hostname,
            Scheme(result.scheme.upper()),
            rawe.url,
            rawe.err is None,
            rawe.queried_when,
            rawe.resp_ans.elapsed.dns_lookup,
            rawe.resp_ans.elapsed.tls_handshake,
            rawe.resp_ans.elapsed.server_process,
            rawe.resp_ans.elapsed.content_transfer,
            check_frequency,
            rawe.resp_ans.content_length,
            rawe.resp_ans.status_code,
            rawe.err,
        )
    else:
        assert rawe.err is not None
        return URLCheckDetails.failed(
            result.hostname,
            Scheme(result.scheme.upper()),
            rawe.url,
            0,
            0,
            0,
            timeout if rawe.timeout else 0,
            check_frequency,
            rawe.err,
            rawe.queried_when,
        )
