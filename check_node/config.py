import logging
import os
from dataclasses import dataclass, fields
from typing import Dict, Optional, cast

import yaml

from check_node.utils import get_project_root


@dataclass(frozen=True)
class DbEnv:
    DB_HOST: str
    DB_PORT: str
    MYSQL_ROOT_PASSWORD: str
    MYSQL_DATABASE: str
    MYSQL_USER: str
    MYSQL_PASSWORD: str
    BATCH_SIZE: str

    @property
    def db_port(self) -> int:
        return int(self.DB_PORT)

    @property
    def batch_size(self) -> int:
        return int(self.BATCH_SIZE)


@dataclass(frozen=True)
class Env:
    RABBIT_URL: Optional[str] = None
    HEAVY_NODE_URL: str = ""
    DEBUG: Optional[str] = None
    LOGGER_STDIO: Optional[str] = None
    REQUEST_TIMEOUT: Optional[str] = None
    LOADBOUND: Optional[str] = None
    FREQS_FILE_PATH: Optional[str] = None
    USE_CRT: Optional[str] = None
    VERIFY_SSL: Optional[str] = None

    @property
    def logger_stdio(self) -> bool:
        return self.LOGGER_STDIO == "True"

    def is_debug(self) -> bool:
        return self.DEBUG == "True"

    def log_level(self) -> int:
        return logging.DEBUG if self.is_debug() else logging.INFO

    @property
    def request_timeout(self) -> int:
        return cast(
            int,
            int(
                self.REQUEST_TIMEOUT
                if self.REQUEST_TIMEOUT is not None
                else "0"
            ),
        )

    @property
    def loadbound(self) -> int:
        return cast(
            int, int(self.LOADBOUND if self.LOADBOUND is not None else "0")
        )

    @property
    def use_crt(self) -> bool:
        return self.USE_CRT == "True"

    @property
    def verify_ssl(self) -> bool:
        return self.VERIFY_SSL == "True"


def dict_from_os_env(from_env: Optional[Dict[str, str]]):
    if from_env is None:
        from dotenv import load_dotenv

        env_path = get_project_root() / ".env"
        db_env_path = get_project_root() / ".db-env"
        load_dotenv(dotenv_path=env_path)
        load_dotenv(dotenv_path=db_env_path)
    else:
        for k, v in from_env.items():
            os.environ[k] = v
    return os.environ


def get_db_env(from_env: Optional[Dict[str, str]] = None) -> DbEnv:
    from_env = dict_from_os_env(from_env)
    try:
        # fmt: off
        return DbEnv(**{field.name: from_env[field.name] for field in fields(DbEnv)})  # type: ignore
        # fmt: on
    except KeyError:
        logging.error("you did not set all the ENV params properly for DbEnv!")
        raise SystemExit()


def get_env(from_env: Optional[Dict[str, str]] = None) -> Env:
    from_env = dict_from_os_env(from_env)
    try:
        # fmt: off
        return Env(**{field.name: from_env[field.name] for field in fields(Env)})  # type: ignore
        # fmt: on
    except KeyError:
        logging.error("you did not set all the ENV params properly for Env!")
        raise SystemExit()


def get_freqs_path() -> str:
    freqs_file_path = os.environ.get("FREQS_FILE_PATH")
    if freqs_file_path is not None:
        return freqs_file_path
    freqs_file_path = get_env().FREQS_FILE_PATH
    if freqs_file_path is not None:
        return freqs_file_path
    else:
        return get_project_root() / ".freq-params.yaml"


def read_freq_params() -> Dict[str, Dict[str, float]]:
    # returns two frequency params, first is low, second is high
    with open(get_freqs_path()) as freqs_file:
        return yaml.safe_load(freqs_file.read())


def write_freq_params(freqs_as_dict: Dict[str, Dict[str, float]]):
    # write two frequency params
    with open(get_freqs_path(), "w") as freqs_file:
        freqs_file.write(yaml.safe_dump(freqs_as_dict))
