# if-kz_check-node

This project is intended to be a request checker 
node for master/controller node of if-kz.


## input & output

All action types are defined in `RabbitActionTypes` enum in [models.py](check_node/rabbit/models.py).


### input

Rabbit queue name:
  - `check:{ASN_CODE} => check:15169`

#### meta calls
 - ##### SET_TIME_INTERVAL => calls::set_time_interval
   ```json
   {
     "action": "SET_TIME_INTERVAL",
     "payload": {
       "check_frequency": "LOW",
       "interval": {
         "weeks"       : 0.0,
         "days"        : 0.0,
         "hours"       : 0.0,
         "minutes"     : 0.0,
         "seconds"     : 0.0,
         "microseconds": 0.0,
         "milliseconds": 0.0
       } 
     }
   }
   ```

 - ##### GET_TIME_INTERVAL => calls::get_time_interval:
   ```json
   {
    "action": "GET_TIME_INTERVAL",
    "payload": {
      "check_frequency": "LOW"
    }
   }
   ```

#### light calls
 - ##### WHOAMI => calls::whoami
   ```json
   {
    "action": "WHOAMI"
   }
   ```

 - ##### GET_URL => calls::check_and_also_log
   sending one would query url in current frame of frequency if it's not 
   available and would set given frequency for a further usage 
   ```json
   {
     "action":  "GET_URL",
     "payload": {
       "url"            : "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
       "check_frequency": "LOW"
     }
   }
    ```

 - ##### DELETE_MATCHING_HOSTNAME => call::delete_matching_hostname
   ```json
   {  
     "action":  "DELETE_MATCHING_HOSTNAME",
     "payload": {
       "hostname": "abayev-did-nothing-wro.ng"
     }
   }
   ```
   
#### heavy calls
 - ##### UPDATE_ALL_FOR_LOW_FREQ => calls::update_all_for_low_freq
   ```json
   {
     "action": "UPDATE_ALL_FOR_LOW_FREQ"
   }
   ```
  
 - ##### UPDATE_ALL_FOR_HIGH_FREQ => calls::update_all_for_high_freq
    I wouldn't consider this one as a heavy call, yet let it be.
   ```json
   {
     "action": "UPDATE_ALL_FOR_HIGH_FREQ"
   }
   ```
 - ##### GET_ALL_URLS => calls::get_all_urls
   ```json
   {
    "action": "GET_ALL_URLS"
   }
   ```
  
### output

We consider master node to have one queue, which is going to be fed by check-nodes.
 - `master`

#### meta calls
 - ##### SET_TIME_INTERVAL => calls::set_time_interval
   ```json
   {
     "asn_id": "15169",
     "action": "SET_TIME_INTERVAL",
     "payload": {
       "check_frequency": "LOW",
       "interval": {
         "weeks"       : 0.0,
         "days"        : 0.0,
         "hours"       : 0.0,
         "minutes"     : 0.0,
         "seconds"     : 0.0,
         "microseconds": 0.0,
         "milliseconds": 0.0
       } 
     }
   }
   ```

 - ##### GET_TIME_INTERVAL => calls::get_time_interval:
   ```json
   {
     "asn_id": "15169",
     "action": "GET_TIME_INTERVAL",
     "payload": {
       "check_frequency": "LOW",
       "interval": {
         "weeks"       : 0.0,
         "days"        : 0.0,
         "hours"       : 0.0,
         "minutes"     : 0.0,
         "seconds"     : 0.0,
         "microseconds": 0.0,
         "milliseconds": 0.0
       } 
     }
   }
   ```

#### light calls
 - ##### WHOAMI => calls::whoami
   ```json
   {
    "asn_id": "15169",
    "action": "WHOAMI",
    "payload": {
     "ip"          : "64.233.165.139",
     "country_code": "US",
     "isp": "GOOGLE, US",
     "asn_id": ""
    }
   }
   ```

 - ##### GET_URL => calls::check_and_also_log
   ```json
   { 
     "asn_id": "15169",
     "action":  "GET_URL",
     "payload": [
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTP",
         "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 0, 
         "status_code": 301,
         "exc": null
       },
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTPS",
         "url": "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 1337, 
         "status_code": 200,
         "exc": null
       }
     ]
   }
    ```

 - ##### DELETE_MATCHING_HOSTNAME => call::delete_matching_hostname
   ```json
   {  
     "asn_id": "15169",
     "action":  "DELETE_MATCHING_HOSTNAME",
     "payload": {
       "hostname": "abayev-did-nothing-wro.ng",
       "deleted": [ {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTP",
         "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 0, 
         "status_code": 301,
         "exc": null
       },
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTPS",
         "url": "https://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 1337, 
         "status_code": 200,
         "exc": null
       }]
     }
   }
   ```
   
#### heavy calls

 - ##### UPDATE_ALL_FOR_LOW_FREQ => calls::get_all_urls
   This call considers sending data in chunks of `BATCH_SIZE` param written in `.db-env`.
   ```json
   {
    "asn_id": "15169", 
    "action": "UPDATE_ALL_FOR_LOW_FREQ", 
    "payload": [
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTP",
         "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 0, 
         "status_code": 301,
         "exc": null
       }, 
       ...
     ]
   }
   ```

 - ##### UPDATE_ALL_FOR_HIGH_FREQ => calls::get_all_urls
   This call considers sending data in chunks of `BATCH_SIZE` param written in `.db-env`.
   ```json
   {
    "asn_id": "15169", 
    "action": "UPDATE_ALL_FOR_HIGH_FREQ", 
    "payload": [
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTP",
         "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 0, 
         "status_code": 301,
         "exc": null
       }, 
       ...
     ]
   }
   ```


 - ##### GET_ALL_URLS => calls::get_all_urls
   This call considers sending data in chunks of `BATCH_SIZE` param written in `.db-env`.
   ```json
   {
    "asn_id": "15169", 
    "action": "GET_ALL_URLS", 
    "payload": [
       {    
         "hostname": "abayev-did-nothing-wro.ng",
         "scheme": "HTTP",
         "url": "http://www.abayev-did-nothing-wro.ng/really?I=swear&to=god",
         "accessible": true,
         "last_check_time_by_utc": "2011-07-14T01:00:00Z",
         "elapsed_dns_lookup": 0.0,
         "elapsed_tls_handshake": 0.0,
         "elapsed_server_process": 0.0,
         "elapsed_content_transfer": 0.9,
         "check_frequency": "LOW",
         "content_length": 0, 
         "status_code": 301,
         "exc": null
       }, 
       ...
     ]
   }
   ```


## methods
Sending HEAD requests, cuz they're fast and cheap.
Prefer setting __User-Agent__ header instead of empty one, since request may end up being an error. 

Yet not sure on timeout section, that's why I prefer hardcoding 30s on regular calls and same for heavy calls.

Currently it doesn't follow redirect. I think for the proper tests it should follow too.  


## production

I expect this project to be launched on raspberry pi, 
thus I'm interested in reducing size/cost/whatever possible.

First clone this repo: 
```shell script
$ git clone git@gitlab.com:ifkz/check-node.git
```

Then you have to install depending on your *nix:
 - docker
 - docker-compose
 
Further set .env variables:
```shell script
$ cp .env.sample .env
$ cp .db-env.sample .db-env
$ cp .freq-params.sample.yaml .freq-params.yaml
$ vim .env
$ vim .db-env
$ vim .freq-params.yaml
```

After which run:
```shell script
$ docker-compose up -d
```

## development

Consider committing into `dev` branch. 
Tests are in `tests` folder

In order to test you have to install the project with 
test-deps without docker I guess(since I personally can't use IDE debugger with docker).
Thus you need:
 - docker
 - docker-compose
 - python3.8
 - pip3


Then you install dependencies:
```shell script
$ python3.8 -m venv venv
$ poetry install
```

Project comes with a precommit hooks for a linting/styling/static type-checking.
 - black
 - autoflake
 - mypy
 - flake8
 
#### testing:
```shell script
$ pytest -v --cov=check_node --cov-report term-missing -cov-fail-under=100 tests/
```

Other tests({style,type}-check) are being run under pre commit hook,
 so all you have to do is to commit.

 
### benchmark

I didn't have a raspberry pi device on hand, hence I couldn't bench it properly.

First thing to notice is a bulk/batch size, I prefer setting it to __6000 entities per call__.
If you want to change then lookup for `BATCH_SIZE` constant in .db-env.

You can see plot images and csv files in `data` folder for each replace/insert operations. 
I expect you to not to stress on `insert`, since batch operations are mostly ~ 99% `replace` ones.
Bench was run on my own laptop which is more performant than avg pi station.

##### insert
![insert ops](data/insert.png "insert")

##### replace
![replace ops](data/replace.png "replace")


## misc
don't hesitate to dm me on [telegram](https://t.me/arpanetus)