from typing import List

import pytest
from aioresponses import aioresponses

from check_node.check import CheckKit
from check_node.db.models import URLCheckDetails
from check_node.models import URLDetails
from check_node.exceptions import URLIsNotValid

pytestmark = pytest.mark.asyncio


async def test_rdap(check_kit: CheckKit, rdap_mock, close_aio):
    resp_rdap_dict = check_kit.rdap("64.233.165.139")
    assert resp_rdap_dict is not None
    await check_kit.session.close()


async def test_whoami(check_kit: CheckKit, rdap_mock, close_aio):
    with aioresponses() as mock_response:
        mock_response.get("https://api.ipify.org/", body="64.233.165.139")
        ip_dets = await check_kit.whoami()
        assert ip_dets is not None
        assert ip_dets.ip == "64.233.165.139"
        assert ip_dets.country_code == "US"
        assert ip_dets.isp == "GOOGLE, US"
        await check_kit.session.close()


async def test_whoami_fail_ipify(check_kit: CheckKit, close_aio):
    with aioresponses() as mock_response:
        mock_response.get("https://api.ipify.org/", body="", status=400)
        ip_dets = await check_kit.whoami()
        assert ip_dets.ip == ""
        assert ip_dets.country_code == ""
        assert ip_dets.isp == ""
        await check_kit.session.close()


def parse_url_assertions(
    not_blocked_url_str,
    url_to_parse_route,
    url_to_parse_http,
    url_to_parse_https,
    url_details,
):
    assert url_details.http_route == url_to_parse_http
    assert url_details.https_route == url_to_parse_https

    assert url_details.hostname == not_blocked_url_str, url_to_parse_route


async def test_parse_url_no_scheme(
    check_kit: CheckKit,
    not_blocked_url_str,
    url_to_parse_route,
    url_to_parse_no_protocol,
    url_to_parse_http,
    url_to_parse_https,
    close_aio,
):
    url_details = check_kit.parse_url(url_to_parse_no_protocol)
    parse_url_assertions(
        not_blocked_url_str,
        url_to_parse_route,
        url_to_parse_http,
        url_to_parse_https,
        url_details,
    )


async def test_parse_url_http(
    check_kit: CheckKit,
    not_blocked_url_str,
    url_to_parse_route,
    url_to_parse_http,
    url_to_parse_https,
    close_aio,
):
    url_details = check_kit.parse_url(url_to_parse_http)
    parse_url_assertions(
        not_blocked_url_str,
        url_to_parse_route,
        url_to_parse_http,
        url_to_parse_https,
        url_details,
    )


async def test_parse_url_https(
    check_kit: CheckKit,
    not_blocked_url_str,
    url_to_parse_route,
    url_to_parse_http,
    url_to_parse_https,
    close_aio,
):
    url_details = check_kit.parse_url(url_to_parse_https)
    parse_url_assertions(
        not_blocked_url_str,
        url_to_parse_route,
        url_to_parse_http,
        url_to_parse_https,
        url_details,
    )


async def test_parse_url_wrong_scheme(
    check_kit: CheckKit, wrong_url_str, close_aio
):
    with pytest.raises(URLIsNotValid):
        check_kit.parse_url(wrong_url_str)


async def test_get_url_ok(
    check_kit: CheckKit,
    response_not_blocked_301,
    response_not_blocked_200,
    url_details_not_blocked: URLDetails,
    close_aio,
):
    check_list: List[URLCheckDetails] = await check_kit.get_url(
        url_details_not_blocked
    )
    for check in check_list:
        assert check.accessible
        assert 0 < check.content_length is not None


async def test_get_url_client_error(
    check_kit: CheckKit,
    response_not_blocked_301,
    response_not_blocked_400,
    url_details_not_blocked: URLDetails,
    close_aio,
):
    check_list: List[URLCheckDetails] = await check_kit.get_url(
        url_details_not_blocked
    )
    for check in check_list:
        assert check.accessible
        assert 0 < check.content_length is not None


async def test_get_url_server_error(
    check_kit: CheckKit,
    response_not_blocked_301,
    response_not_blocked_500,
    url_details_not_blocked: URLDetails,
    close_aio,
):
    check_list: List[URLCheckDetails] = await check_kit.get_url(
        url_details_not_blocked
    )
    for check in check_list:
        assert check.accessible
        assert 0 < check.content_length is not None


async def test_get_url_fail(
    check_kit: CheckKit,
    response_blocked_http,
    response_blocked_https,
    url_details_blocked: URLDetails,
    close_aio,
):
    check_list: List[URLCheckDetails] = await check_kit.get_url(
        url_details_blocked
    )
    for check in check_list:
        assert not check.accessible
