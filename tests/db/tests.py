from dataclasses import fields
from typing import List, Tuple, Set

import pytest
from sqlalchemy import Table

from check_node.db.manager import DBManager, TableName
from check_node.config import DbEnv
from check_node.db.models import URLCheckDetails
from tests import close_aiomysql_conn

pytestmark = pytest.mark.asyncio


async def test_check_connection_db_ok():
    db_man = DBManager()
    await db_man.set_engine()
    assert db_man._engine is not None
    await close_aiomysql_conn(db_man)


async def test_check_connection_db_fail(fail_db_env: DbEnv, close_aio):
    db_man = DBManager(fail_db_env)
    with pytest.raises(SystemExit):
        await db_man.set_engine()
    assert db_man._engine is None


async def test_init_tables(db_manager_set_engine: DBManager, close_aio):
    await db_manager_set_engine.init_tables()
    create_list = [
        db_manager_set_engine.table_dict[every_table_key]
        for every_table_key in db_manager_set_engine.table_dict
    ]
    assert create_list is not None
    assert len(create_list) > 0
    assert (
        db_manager_set_engine.table_dict[TableName.URL_CHECK_DETAILS].table
        is not None
    )
    url_details_table: Table = db_manager_set_engine.table_dict[
        TableName.URL_CHECK_DETAILS
    ].table
    for field in fields(URLCheckDetails):
        assert getattr(url_details_table.columns, field.name) is not None
    await close_aiomysql_conn(db_manager_set_engine)


async def test_drop_tables(db_manager_init_tables: DBManager, close_aio):
    await db_manager_init_tables.drop_all_tables()
    await close_aiomysql_conn(db_manager_init_tables)


async def test_create_tables(db_manager_drop_all_tables: DBManager, close_aio):
    await db_manager_drop_all_tables.create_tables()
    await close_aiomysql_conn(db_manager_drop_all_tables)


async def test_insert_punycode_url(
    db_manager_create_tables: DBManager, punycode_url_check_details, close_aio
):
    rs = await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, punycode_url_check_details
    )
    assert len(rs) == 1
    count = await db_manager_create_tables.count(TableName.URL_CHECK_DETAILS)
    assert 1 == count
    await close_aiomysql_conn(db_manager_create_tables)


async def test_insert_url_failed_check_details(
    db_manager_create_tables: DBManager,
    list_failed_of_url_check_details,
    close_aio,
):
    rs = await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, list_failed_of_url_check_details
    )
    assert len(rs) == 1
    count = await db_manager_create_tables.count(TableName.URL_CHECK_DETAILS)
    assert 2 == count
    await close_aiomysql_conn(db_manager_create_tables)


async def test_insert_url_successful_check_details(
    db_manager_create_tables: DBManager,
    list_successful_of_url_check_details,
    close_aio,
):
    rs = await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, list_successful_of_url_check_details
    )
    assert len(rs) == 1
    count = await db_manager_create_tables.count(TableName.URL_CHECK_DETAILS)
    assert 2 == count
    await close_aiomysql_conn(db_manager_create_tables)


async def test_select_where_check_details(
    db_manager_inserted: Tuple[
        DBManager, List[URLCheckDetails], List[URLCheckDetails]
    ],
    low_and_high_clauses,
    close_aio,
):
    got_failed_ucds: List[URLCheckDetails] = await db_manager_inserted[
        0
    ].select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails, low_and_high_clauses[0]
    )
    got_successful_ucds: List[URLCheckDetails] = await db_manager_inserted[
        0
    ].select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails, low_and_high_clauses[1]
    )
    assert {hash(i) for i in got_failed_ucds} == {
        hash(i) for i in db_manager_inserted[1]
    }
    assert {hash(i) for i in got_successful_ucds} == {
        hash(i) for i in db_manager_inserted[2]
    }
    await close_aiomysql_conn(db_manager_inserted[0])


async def test_count_all_check_details(
    db_manager_inserted: Tuple[
        DBManager, List[URLCheckDetails], List[URLCheckDetails]
    ],
    low_and_high_clauses,
    close_aio,
):
    assert 4 == await db_manager_inserted[0].count(TableName.URL_CHECK_DETAILS)

    assert 2 == await db_manager_inserted[0].count(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[0]
    )

    assert 2 == await db_manager_inserted[0].count(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[0]
    )

    await db_manager_inserted[0].delete(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[0]
    )

    assert 0 == await db_manager_inserted[0].count(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[0]
    )

    await db_manager_inserted[0].delete(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[1]
    )

    assert 0 == await db_manager_inserted[0].count(TableName.URL_CHECK_DETAILS)
    await close_aiomysql_conn(db_manager_inserted[0])


async def test_delete_where_check_details(
    db_manager_inserted: Tuple[
        DBManager, List[URLCheckDetails], List[URLCheckDetails]
    ],
    low_and_high_clauses,
    close_aio,
):
    await db_manager_inserted[0].delete(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[0]
    )
    got_failed_ucds: List[URLCheckDetails] = await db_manager_inserted[
        0
    ].select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails, low_and_high_clauses[0]
    )

    await db_manager_inserted[0].delete(
        TableName.URL_CHECK_DETAILS, low_and_high_clauses[1]
    )
    got_successful_ucds: List[URLCheckDetails] = await db_manager_inserted[
        0
    ].select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails, low_and_high_clauses[1]
    )
    assert set(got_failed_ucds) != set(db_manager_inserted[1])
    assert set(got_successful_ucds) != set(db_manager_inserted[2])
    await close_aiomysql_conn(db_manager_inserted[0])


async def test_batch_insert(
    db_manager_create_tables: DBManager,
    generated_ucds_for_batch_insert: List[URLCheckDetails],
):
    rs = await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, generated_ucds_for_batch_insert
    )
    assert len(rs) == 5
    count = await db_manager_create_tables.count(TableName.URL_CHECK_DETAILS)
    assert 32000 == count
    await close_aiomysql_conn(db_manager_create_tables)


async def test_batch_select(
    db_manager_batch_insert: Tuple[DBManager, List[URLCheckDetails]]
):
    hashed = {hash(i) for i in db_manager_batch_insert[1]}
    counts: List[int] = []
    new_hash: Set[int] = set()
    async for entries in db_manager_batch_insert[0].batch_select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    ):
        new_hash = new_hash | {hash(i) for i in entries}
        counts.append(len(entries))

    assert hashed == new_hash
    assert len(counts) == 5
    assert counts == [7000, 7000, 7000, 7000, 4000]
    await close_aiomysql_conn(db_manager_batch_insert[0])
