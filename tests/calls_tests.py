import dataclasses
from dataclasses import replace
from typing import List, cast

import pytest
from aioresponses import aioresponses

from check_node.calls import LightCalls, RabbitCalls
from check_node.check import CheckKit
from check_node.db.manager import DBManager, TableName
from check_node.db.models import URLCheckDetails
from check_node.exceptions import UnparseableEntity
from check_node.rabbit.models import OutMessage
from tests import close_aiomysql_conn

pytestmark = pytest.mark.asyncio


async def l_assert_log(db_manager, check_kit, url_str, accessible):
    lc = LightCalls(db_manager, check_kit)
    await lc.l_check_also_log(url_str)
    response = await db_manager.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    assert len(response) == 2
    assert response[0].hostname == url_str
    assert response[1].hostname == url_str
    assert response[0].accessible == accessible
    assert response[1].accessible == accessible
    await close_aiomysql_conn(db_manager)


@pytest.mark.asyncio
async def test_l_check_also_log_succesful(
    db_manager_create_tables: DBManager,
    not_blocked_url_str,
    response_not_blocked_301,
    response_not_blocked_200,
    check_kit: CheckKit,
):
    await l_assert_log(
        db_manager_create_tables, check_kit, not_blocked_url_str, True
    )


async def test_l_check_also_log_for_blocked(
    db_manager_create_tables: DBManager,
    blocked_url_str,
    response_blocked_http,
    response_blocked_https,
    check_kit: CheckKit,
):
    await l_assert_log(
        db_manager_create_tables, check_kit, blocked_url_str, False
    )


async def test_l_check_also_log_was_in_interval(
    db_manager_create_tables: DBManager,
    not_blocked_url_str,
    response_not_blocked_200,
    response_not_blocked_301,
    check_kit: CheckKit,
):
    lc = LightCalls(db_manager_create_tables, check_kit)
    await lc.l_check_also_log(not_blocked_url_str)
    early_response = await db_manager_create_tables.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    await lc.l_check_also_log(not_blocked_url_str)
    response = await db_manager_create_tables.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    assert early_response == response
    await close_aiomysql_conn(db_manager_create_tables)


async def test_l_check_also_log_expired_from_interval(
    db_manager_create_tables: DBManager,
    not_blocked_url_str,
    response_not_blocked_200,
    response_not_blocked_301,
    response_not_blocked_200_d,
    response_not_blocked_301_d,
    check_kit: CheckKit,
):
    lc = LightCalls(db_manager_create_tables, check_kit)
    await lc.l_check_also_log(not_blocked_url_str)
    early_response = await db_manager_create_tables.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    f_resp = early_response[0]
    actual_date = f_resp.last_check_time_by_utc
    interval_timedelta = lc.m_get_time_interval(
        f_resp.check_frequency
    ).as_timedelta()
    expired_date = actual_date - interval_timedelta
    ucd_to_insert = replace(f_resp, last_check_time_by_utc=expired_date)
    await db_manager_create_tables.insert(
        TableName.URL_CHECK_DETAILS, [ucd_to_insert]
    )
    early_response = await db_manager_create_tables.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    await lc.l_check_also_log(not_blocked_url_str)
    response = await db_manager_create_tables.select(
        TableName.URL_CHECK_DETAILS, URLCheckDetails
    )
    assert early_response != response
    await close_aiomysql_conn(db_manager_create_tables)


async def test_rabbit_calls_set_time_interval(
    rabbit_calls: RabbitCalls,
    in_message_set_time_interval,
    out_message_set_time_interval,
):
    got_out_message = await rabbit_calls.respond(in_message_set_time_interval)
    assert got_out_message == out_message_set_time_interval
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_calls_get_time_interval(
    rabbit_calls: RabbitCalls,
    in_message_get_time_interval,
    out_message_get_time_interval,
):
    got_out_message = await rabbit_calls.respond(in_message_get_time_interval)
    assert got_out_message == out_message_get_time_interval
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_calls_whoami(
    rabbit_calls: RabbitCalls, in_message_whoami, out_message_whoami, rdap_mock
):
    with aioresponses() as mock_response:
        mock_response.get("https://api.ipify.org/", body="64.233.165.139")
        got_out_message = await rabbit_calls.respond(in_message_whoami)
        assert got_out_message == out_message_whoami
        await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_calls_get_url(
    rabbit_calls: RabbitCalls,
    in_message_get_url,
    out_message_get_url,
    rabbit_response_not_blocked_301,
    rabbit_response_not_blocked_200,
):
    fields = (
        "hostname",
        "scheme",
        "url",
        "accessible",
        "check_frequency",
        "content_length",
        "status_code",
        "exc",
    )
    got_out_message = await rabbit_calls.respond(in_message_get_url)
    assert got_out_message.action == out_message_get_url.action
    for f, s in zip(
        cast(List[URLCheckDetails], got_out_message.payload),
        cast(List[URLCheckDetails], out_message_get_url.payload),
    ):
        for field in fields:
            assert getattr(f, field) == getattr(s, field)
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_calls_delete_matching_hostname(
    rabbit_calls: RabbitCalls,
    in_message_delete_matching_hostname,
    out_message_delete_matching_hostname,
    default_ucds,
):
    await rabbit_calls.heavy_calls.db_manager.insert(
        TableName.URL_CHECK_DETAILS, default_ucds
    )
    got_out_message = await rabbit_calls.respond(
        in_message_delete_matching_hostname
    )
    assert out_message_delete_matching_hostname == got_out_message
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_update_all_for_low_freq(
    rabbit_calls: RabbitCalls,
    in_message_update_for_all_low_freq,
    out_message_update_all_for_low_freq,
    default_ucds,
    rabbit_heavy_response,
):
    got_out_message = OutMessage(
        out_message_update_all_for_low_freq.asn_id,
        out_message_update_all_for_low_freq.action,
        [],
    )

    await rabbit_calls.heavy_calls.db_manager.insert(
        TableName.URL_CHECK_DETAILS, default_ucds
    )

    async for got_out_message_chunk in rabbit_calls.heavy_respond(
        in_message_update_for_all_low_freq
    ):
        cast(List[URLCheckDetails], got_out_message.payload).extend(
            cast(List[URLCheckDetails], got_out_message_chunk.payload)
        )

    assert out_message_update_all_for_low_freq == got_out_message
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_update_all_for_high_freq(
    rabbit_calls: RabbitCalls,
    in_message_update_for_all_high_freq,
    out_message_update_all_for_high_freq,
    default_high_ucds,
    rabbit_heavy_response,
):
    got_out_message = OutMessage(
        out_message_update_all_for_high_freq.asn_id,
        out_message_update_all_for_high_freq.action,
        [],
    )

    await rabbit_calls.heavy_calls.db_manager.insert(
        TableName.URL_CHECK_DETAILS, default_high_ucds
    )

    async for got_out_message_chunk in rabbit_calls.heavy_respond(
        in_message_update_for_all_high_freq
    ):
        cast(List[URLCheckDetails], got_out_message.payload).extend(
            cast(List[URLCheckDetails], got_out_message_chunk.payload)
        )

    assert out_message_update_all_for_high_freq == got_out_message
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_get_all(
    rabbit_calls: RabbitCalls,
    in_message_get_all,
    out_message_get_all,
    default_ucds,
):
    got_out_message = OutMessage(
        out_message_get_all.asn_id, out_message_get_all.action, []
    )

    await rabbit_calls.heavy_calls.db_manager.insert(
        TableName.URL_CHECK_DETAILS, default_ucds
    )

    async for got_out_message_chunk in rabbit_calls.heavy_respond(
        in_message_get_all
    ):
        cast(List[URLCheckDetails], got_out_message.payload).extend(
            cast(List[URLCheckDetails], got_out_message_chunk.payload)
        )

    assert out_message_get_all == got_out_message
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_unparseable_cast(
    rabbit_calls: RabbitCalls, in_message_get_url
):
    in_message_get_url = dataclasses.replace(in_message_get_url, payload=None)

    with pytest.raises(UnparseableEntity):
        await rabbit_calls.respond(in_message_get_url)
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_unparseable_respond(
    rabbit_calls: RabbitCalls, in_message_get_all
):
    with pytest.raises(UnparseableEntity):
        await rabbit_calls.respond(in_message_get_all)
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)


async def test_rabbit_unparseable_heavy_respond(
    rabbit_calls: RabbitCalls, in_message_get_url
):
    with pytest.raises(UnparseableEntity):
        async for _ in rabbit_calls.heavy_respond(in_message_get_url):
            pass
    await close_aiomysql_conn(rabbit_calls.heavy_calls.db_manager)
