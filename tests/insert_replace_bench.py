import os
import time
import uuid
import sys
from random import randint
from typing import List
import asyncio

from check_node.config import get_db_env
from check_node.db.manager import DBManager, TableName
from check_node.db.models import URLCheckDetails, Scheme, CheckFrequency
from check_node.utils import get_datetime_now_by_utc, get_project_root


class Timer:
    def __init__(self, action, num, file_path):
        self.action = action
        self.num = num
        self.file_path = file_path
        self.first = time.perf_counter()

    def _stop(self):
        self.last = time.perf_counter()
        done = self.last - self.first
        with open(self.file_path, "a+") as file:
            file.write(f"{self.action};{self.num};{done:.2f}\n")
        print(f"{self.action}\t{self.num}\t\t{done:.2f}s")

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._stop()


uuids = [uuid.uuid4().__str__() for i in range(50001)]


def generate(number: int, insert_new) -> List[URLCheckDetails]:
    return [
        URLCheckDetails(
            uuid.uuid4().__str__() if insert_new else uuids[randint(0, 50000)],
            Scheme.HTTP,
            "that goddamn website",
            True,
            get_datetime_now_by_utc(),
            0,
            0,
            0,
            1.0,
            CheckFrequency.LOW,
            1,
            2,
        )
        for _ in range(number)
    ]


async def endless_select(insert_new):
    db_man = DBManager(get_db_env())
    await db_man.set_engine()
    await db_man.init_tables()
    await db_man.create_tables()
    table_name = TableName.URL_CHECK_DETAILS
    column = db_man.c_dict[TableName.URL_CHECK_DETAILS].check_frequency
    where_clause = column == CheckFrequency.LOW
    report_path = get_project_root() / "data/report.csv"
    os.remove(report_path)
    with open(report_path, "a+") as file:
        file.write("action;entries;timing\n")
    for idx in range(1000):
        print(f"\n\nITERATION NUM: {idx}\n\n")
        num = randint(1000, 50000)
        ucds = generate(num, insert_new)
        insert_str = "INSERT"
        if not insert_new:
            insert_str = "REPLACE"
        with Timer(insert_str, num, report_path):
            await db_man.insert(table_name, ucds)
        with Timer("SELECT", num, report_path):
            await db_man.select(table_name, URLCheckDetails)
        if insert_new:
            with Timer("DELETE", num, report_path):
                await db_man.delete(table_name, where_clause)


if __name__ == "__main__":
    insert = True if sys.argv[1] == "insert-new" else False
    asyncio.run(endless_select(insert))
