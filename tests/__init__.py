from check_node.db.manager import DBManager


async def close_aiomysql_conn(db_manager: DBManager):
    assert db_manager.engine is not None
    db_manager.engine.close()
    await db_manager.engine.wait_closed()
