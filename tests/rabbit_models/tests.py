import jsons

from check_node.rabbit.models import InMessage, OutMessage


def test_parse_in_set_time_interval(
    in_str_set_time_interval, in_message_set_time_interval: InMessage
):
    assert in_message_set_time_interval == jsons.loads(
        in_str_set_time_interval, InMessage
    )


def test_parse_in_get_time_interval(
    in_str_get_time_interval, in_message_get_time_interval: InMessage
):
    assert in_message_get_time_interval == jsons.loads(
        in_str_get_time_interval, InMessage
    )


def test_parse_in_whoami(in_str_whoami, in_message_whoami: InMessage):
    assert in_message_whoami == jsons.loads(in_str_whoami, InMessage)


def test_parse_in_get_url(in_str_get_url, in_message_get_url: InMessage):
    assert in_message_get_url == jsons.loads(in_str_get_url, InMessage)


def test_parse_in_delete_matching_hostname(
    in_str_delete_matching_hostname: str,
    in_message_delete_matching_hostname: InMessage,
):
    assert in_message_delete_matching_hostname == jsons.loads(
        in_str_delete_matching_hostname, InMessage
    )


def test_parse_in_update_for_all_low_freq(
    in_str_update_for_all_low_freq: str,
    in_message_update_for_all_low_freq: InMessage,
):
    assert in_message_update_for_all_low_freq == jsons.loads(
        in_str_update_for_all_low_freq, InMessage
    )


def test_parse_in_update_for_all_high_freq(
    in_str_update_for_all_high_freq: str,
    in_message_update_for_all_high_freq: InMessage,
):
    assert in_message_update_for_all_high_freq == jsons.loads(
        in_str_update_for_all_high_freq, InMessage
    )


def test_parse_in_get_all(in_str_get_all: str, in_message_get_all: InMessage):
    assert in_message_get_all == jsons.loads(in_str_get_all, InMessage)


def test_parse_out_set_interval(
    out_message_set_time_interval: OutMessage, out_str_set_time_interval: str
):
    assert jsons.loads(out_str_set_time_interval) == jsons.dump(
        out_message_set_time_interval
    )


def test_parse_out_get_interval(
    out_message_get_time_interval: OutMessage, out_str_get_time_interval: str
):
    assert jsons.loads(out_str_get_time_interval) == jsons.dump(
        out_message_get_time_interval
    )


def test_parse_out_whoami(out_message_whoami: OutMessage, out_str_whoami: str):
    assert jsons.loads(out_str_whoami) == jsons.dump(out_message_whoami)


def test_parse_out_get_url(
    out_message_get_url: OutMessage, out_str_get_url: str
):
    assert jsons.dump(jsons.loads(out_str_get_url)) == jsons.dump(
        out_message_get_url
    )


def test_parse_out_delete_matching_hostname(
    out_message_delete_matching_hostname: OutMessage,
    out_str_delete_matching_hostname: str,
):
    assert jsons.dump(
        jsons.loads(out_str_delete_matching_hostname)
    ) == jsons.dump(out_message_delete_matching_hostname)


def test_parse_out_get_all(
    out_message_get_all: OutMessage, out_str_get_all: str
):
    assert jsons.dump(jsons.loads(out_str_get_all)) == jsons.dump(
        out_message_get_all
    )


def test_parse_out_update_all_for_high_freq(
    out_message_update_all_for_high_freq: OutMessage,
    out_str_update_all_for_high_freq: str,
):
    assert jsons.dump(
        jsons.loads(out_str_update_all_for_high_freq)
    ) == jsons.dump(out_message_update_all_for_high_freq)
